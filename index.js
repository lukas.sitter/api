const Joi = require('joi');
const express = require('express');

const app = express();

app.use(express.json());

const validateCourse = (course) => {
  const schema = Joi.object({
    id: Joi.number(),
    name: Joi.string().min(3).required(),
  });

  return schema.validate(course);
};

const courses = [
  { id: 1, name: 'courses' },
  { id: 2, name: 'horses' },
  { id: 3, name: 'corpses' },
];

app.get('/', (req, res) => {
  res.send('Hello World!!!');
});

app.get('/api/courses', (reg, res) => {
  res.send(courses);
});

app.post('/api/courses', (req, res) => {
  const { error } = validateCourse(req.body);
  if (error) { return res.status(400).send(error.details[0].message); }

  const course = {
    id: courses.length + 1,
    name: req.body.name,
  };
  courses.push(course);
  return (res.send(course));
});

app.get('/api/courses/:id', (req, res) => {
  const course = courses.find((c) => c.id === parseInt(req.params.id, 10));
  if (!course) return res.status(404).send('The course with given id was not found');
  return (res.send(course));
});

app.put('/api/courses/:id', (req, res) => {
  const course = courses.find((c) => c.id === parseInt(req.params.id, 10));
  if (!course) {
    return res.status(404).send('The course with given id was not found');
  }

  const { error } = validateCourse(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  course.name = req.body.name;
  return (res.send(course));
});

app.delete('/api/courses/:id', (req, res) => {
  const course = courses.find((c) => c.id === parseInt(req.params.id, 10));
  if (!course) return res.status(404).send('The course with given id was not found');

  const index = courses.indexOf(course);
  courses.splice(index, 1);

  return (res.send(course));
});

const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`Listening on port ${port}...`));
